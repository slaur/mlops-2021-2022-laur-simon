# import bottle
import spacy
import logging
from json import dumps
from typing import Optional
from fastapi import FastAPI
from fastapi.responses import JSONResponse
import uvicorn



logging.info("Loading model..")
nlp = spacy.load("./models")

app = FastAPI()


# @app.get("/")
# def read_root():
#     return {"Hello": "World"}


# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: Optional[str] = None):
#     return {"item_id": item_id, "q": q}

# print(dict(bottle.request.query.decode()))


@app.get("/api/intent/{sentence}")
# @bottle.route("/api/intent/<sentence>")
def intent_inference(sentence) :
    #sentence = bottle.request.query['sentence']
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    # bottle.response.content_type = "application/json"
    return dumps(inference.cats)

#print("aled",bottle.request.body.read('api/intent?sentence=test'))

# print('intent_inference()', intent_inference())

@app.get("/api/intent-supported-languages")
# @bottle.route("/api/intent-supported-languages")
def supported_languages():
    # bottle.response.content_type = "application/json"
    return dumps(["fr-FR"])

# print('supported_languages()', supported_languages())
# print("test", dumps(["blabla"]))
# if supported_languages()==dumps(["fr-FR"]):
#     print('oui')

@app.get("/health")
# @bottle.route("/health")
def health_check_endpoint():
    return JSONResponse(content=None, status_code=200)

# print('health_check_endpoint()', type(health_check_endpoint()))
# if health_check_endpoint().status == bottle.HTTPResponse(status=200).status:
#     print('coucou')

# print(__name__)

if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8080)
# if __name__ == '__main__':
    # bottle.run(bottle.app(), host='localhost', port=8080, debug=True, reloader=True)