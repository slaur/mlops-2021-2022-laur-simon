FROM docker:19.03.1

COPY requirements.txt .
RUN pip install -r requirements.txt

ADD . /app/
WORKDIR /app